from game import (get_user_choice, get_system_choice,
                  find_winner, update_scoreboard)
from banner import show_result


def play_again():
    """
    get and validate user input, recursively
    and play again game
    :return:
    """
    user_input = input("Do you want to play again? (y/n)")

    if user_input.lower() not in ("y", "n"):
        print("Oops!!, wrong choice, try again please...")
        return play_again()

    if user_input.lower() == "y":
        return play()


def play():
    """
    main play ground handler
    :return:
    """

    result = {
        "user": 0,
        "system": 0
    }

    while result["user"] != 3 and result["system"] != 3:
        user_choice = get_user_choice()
        system_choice = get_system_choice()
        winner = find_winner(user_choice, system_choice)

        if winner == user_choice:
            msg = "You Win"
            result["user"] += 1
        elif winner == system_choice:
            msg = "You Lose"
            result["system"] += 1
        else:
            msg = "Draw"

        show_result(user_choice, system_choice, msg)

    update_scoreboard(result)

    play_again()


if __name__ == '__main__':
    play()
