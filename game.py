import random

from config import GAME_CHOICES, RULES, scoreboard


def get_user_choice():
    """
    get and validate user choice, recursively
    :return: str
    """
    user_input = input("Enter your choice please (r, p ,s): ")
    if user_input not in GAME_CHOICES:
        print("Oops!!, wrong choice, try again please ...")
        return get_user_choice()
    return user_input


def get_system_choice():
    """
    choice random from GAME_CHOICES
    :return: str
    """
    return random.choice(GAME_CHOICES)


def find_winner(user, system):
    """
    receive user and system choice,
    sort them and compare with game rules
    if they are not the same.
    :param user: str
    :param system: str
    :return: str or None
    """
    match = {user, system}
    if len(match) == 1:  # The choice of user and system is the same
        return None

    match = tuple(sorted(match))
    return RULES.get(match)


def update_scoreboard(result):
    """
    update scoreboard after each hand of the game and show live result,
    until now + last hand result
    :param result:
    :return:
    """
    if result.get("user") == 3:
        scoreboard["user"] += 1
        msg = "You Win"
    else:
        scoreboard["system"] += 1
        msg = "You Lose"

    print("#" * 30)
    print("##", f'user: {scoreboard["user"]}'.ljust(24), "##")
    print("##", f'system: {scoreboard["system"]}'.ljust(24), "##")
    print("##", f'last game: {msg}'.ljust(24), "##")
    print("#" * 30)
