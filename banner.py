def show_result(user, system, msg):
    """
    get user and system choice, show result game
    :param user: str
    :param system: str
    :param msg: str
    :return: None
    """
    print(f"User: '{user}'\tSystem: '{system}'\tResult: {msg}")
